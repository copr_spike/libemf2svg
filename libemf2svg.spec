%global commit b583060d5039d915a453d796d644d42fc68d7981
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Summary:       Microsoft EMF (Enhanced Metafile) to SVG conversion library
Name:          libemf2svg
Version:       1.1.0
Release:       3.20190202git%{shortcommit}%{?dist}
License:       GPLv2+
Source:        https://github.com/kakwa/%{name}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
URL:           https://github.com/kakwa/libemf2svg
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: libpng-devel
BuildRequires: zlib-devel
BuildRequires: freetype-devel
BuildRequires: fontconfig-devel

%description
Microsoft EMF (Enhanced Metafile) to SVG conversion library

%package devel
Summary:       Development files for libemf2svg
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description devel
The libemf2svg-devel package contains header files and documentation necessary
for developing programs using the emf2svg library.

%prep
%autosetup -n %{name}-%{commit}

%build
%cmake .
%cmake_build

%install
%cmake_install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license LICENSE
%doc README.md
%{_bindir}/emf2svg-conv
%{_libdir}/libemf2svg.so.1
%{_libdir}/libemf2svg.so.%{version}

%files devel
%{_libdir}/libemf2svg.so
%{_includedir}/emf2svg.h

%changelog
* Sat Feb 2 2019 spike <spike@fedoraproject.org> 1.1.0-3.20190202gitb583060
- Updated to latest git commit

* Sun May 13 2018 spike <spike@fedoraproject.org> 1.1.0-2.20180513gitdd0778f
- Updated to latest git commit

* Tue Apr 24 2018 spike <spike@fedoraproject.org> 1.1.0-1.20180102git6aabe89
- Updated to 1.1.0 release

* Thu Nov 30 2017 spike <spike@fedoraproject.org> 1.0.3-1.20171130git7b3e22e
- Initial package
